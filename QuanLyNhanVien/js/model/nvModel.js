function NhanVien(
  _taiKhoan,
  _ten,
  _email,
  _matKhau,
  _ngayLam,
  _luongCB,
  _chucVu,
  _gioLam
) {
  this.taiKhoan = _taiKhoan;
  this.ten = _ten;
  this.email = _email;
  this.matKhau = _matKhau;
  this.ngayLam = _ngayLam;
  this.luongCB = _luongCB;
  this.chucVu = _chucVu;
  this.gioLam = _gioLam;
  this.tongLuong = function () {
    let totalSalary = 0;

    if (this.chucVu == "Sếp") {
      totalSalary = this.luongCB * 3;
    } else if (this.chucVu == "Trưởng phòng") {
      totalSalary = this.luongCB * 2;
    } else if (this.chucVu == "Nhân viên") {
      totalSalary = this.luongCB * 1;
    }
    return totalSalary;

    // +nếu chức vụ là giám đốc: tổng lương = lương cơ bản * 3
    // +nếu chức vụ là trưởng phòng: tổng lương = lương cơ bản * 2
    // +nếu chức vụ là nhân viên: tổng lương = lương cơ bản * 1
  };
  this.xepLoai = function () {
    let rank = "";
    if (this.gioLam >= 192) {
      rank = "nhân viên xuất sắc";
    } else if (this.gioLam >= 176) {
      rank = "nhân viên giỏi";
    } else if (this.gioLam >= 160) {
      rank = "nhân viên khá";
    } else {
      rank = "nhân viên trung bình";
    }
    return rank;
  };
}
// Xây dựng phương thức xếp loại cho đối tượng nhân viên:
// +nếu nhân viên có giờ làm trên 192h (>=192): nhân viên xuất sắc
// +nếu nhân viên có giờ làm trên 176h (>=176): nhân viên giỏi
// +nếu nhân viên có giờ làm trên 160h (>=160): nhân viên khá
// +nếu nhân viên có giờ làm dưới 160h: nhân viên trung bình
