function kiemTraRong(value, idError) {
  if (value.length == 0) {
    document.getElementById(idError).innerText = "Không để trống";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    return true;
  }
}

function kiemTraTaiKhoan(idNv, listNv, idError) {
  var index = listNv.findIndex(function (nv) {
    return nv.taiKhoan == idNv;
  });
  console.log("index: ", index);
  if (index == -1) {
    document.getElementById(idError).innerText = "";
    return true;
  } else {
    document.getElementById(idError).innerText = "Tài khoản đã tồn tại";
    return false;
  }
}

function kiemTraEmail(value, idError) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  var isEmail = re.test(value);
  if (!isEmail) {
    document.getElementById(idError).innerText = "Email không hợp lệ";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    return true;
  }
}
