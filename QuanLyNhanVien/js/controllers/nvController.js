function layThongTinTuForm() {
  var taiKhoan = document.getElementById("tknv").value.trim();
  var ten = document.getElementById("name").value.trim();
  var email = document.getElementById("email").value.trim();
  var matKhau = document.getElementById("password").value.trim();
  var ngayLam = document.getElementById("datepicker").value.trim();
  var luongCB = document.getElementById("luongCB").value.trim();
  var chucVu = document.getElementById("chucvu").value.trim();
  var gioLam = document.getElementById("gioLam").value.trim();

  var nv = new NhanVien(
    taiKhoan,
    ten,
    email,
    matKhau,
    ngayLam,
    luongCB,
    chucVu,
    gioLam
  );
  return nv;
}
function renderDsnv(list) {
  var contentHTML = "";
  for (var i = 0; i < list.length; i++) {
    var currentNv = list[i];
    var contentTr = `<tr>
    <td>${currentNv.taiKhoan}</td>
    <td>${currentNv.ten}</td>
    <td>${currentNv.email}</td>
    <td>${currentNv.ngayLam}</td>
    <td>${currentNv.chucVu}</td>
    <td>${currentNv.tongLuong()}</td>
    <td>${currentNv.xepLoai()}</td>
    <td>
    <button onclick="xoaNv('${
      currentNv.taiKhoan
    }')" class="btn btn-danger">Xóa</button>
    </td>
    </tr>`;
    contentHTML += contentTr;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}
function showThongTinTuForm(nhanVien) {
  document.getElementById("tknv").value = nhanVien.taiKhoan;
  document.getElementById("name").value = nhanVien.ten;
  document.getElementById("email").value = nhanVien.email;
  document.getElementById("password").value = nhanVien.matKhau;
  document.getElementById("datepicker").value = nhanVien.ngayLam;
  document.getElementById("luongCB").value = nhanVien.luongCB;
  document.getElementById("chucvu").value = nhanVien.chucVu;
  document.getElementById("gioLam").value = nhanVien.gioLam;
}
function resetForm() {
  document.getElementById("formQLNV").reset();
}
