const DSNV = "DSNV";
var dsnv = [];
var dataJson = localStorage.getItem(DSNV);
if (dataJson) {
  var dataRaw = JSON.parse(dataJson);
  dsnv = dataRaw.map(function (item) {
    return new NhanVien(
      item.taiKhoan,
      item.ten,
      item.email,
      item.matKhau,
      item.ngayLam,
      item.luongCB,
      item.gioLam
    );
  });
  renderDsnv(dsnv);
}
function saveLocalStorage() {
  var dsnvJson = JSON.stringify(dsnv);
  localStorage.setItem(DSNV, dsnvJson);
}

function themNv() {
  var newNv = layThongTinTuForm();
  console.log("newNv: ", newNv);
  var isValid = true;
  // kiem tra tai khoan
  isValid =
    isValid & kiemTraRong(newNv.taiKhoan, "tbTKNV") &&
    kiemTraTaiKhoan(newNv.taiKhoan, dsnv, "tbTKNV");
  // kiem tra ten
  isValid = isValid & kiemTraRong(newNv.ten, "tbTen");
  // kiem tra email
  isValid &=
    kiemTraRong(newNv.email, "tbEmail") && kiemTraEmail(newNv.email, "tbEmail");

  if (isValid) {
    dsnv.push(newNv);
    saveLocalStorage();
    renderDsnv(dsnv);
    resetForm();
  }
}
function xoaNv(idNV) {
  var index = dsnv.findIndex(function (nv) {
    return nv.taiKhoan == idNV;
  });
  if (index == -1) return;
  dsnv.splice(index, 1);
  renderDsnv(dsnv);
  saveLocalStorage();
}
function capNhatNv() {
  var edit = layThongTinTuForm();
  var index = dsnv.findIndex(function (nv) {
    return (nv.taiKhoan = edit.taiKhoan);
  });
  if (index == -1) return;
  dsnv[index] = edit;
  saveLocalStorage();
  renderDsnv(dsnv);
  resetForm();
  document.getElementById("tknv").disabled = false;
}
function timNv() {
  let valueSearchInput = document.getElementById("searchName").value;
  let userSearch = dsnv.filter((value) => {
    return value.ten.toUpperCase().includes(valueSearchInput.toUpperCase());
  });
  renderDsnv(userSearch);
}
